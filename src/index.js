const WebSocketServer = require("ws").Server;
const _ = require('lodash');

const channels = ['channel_1', 'channel_2', 'channel_3'];
const messages = ['message_1', 'message_2', 'message_3', 'message_4', 'message_5', 'message_6'];

const wss = new WebSocketServer({
    port: 3000
});

wss.on("connection", (ws) => {
    console.log("WS connected. Listening on port 3000");

    ws.send(JSON.stringify({channel: 'start', msg: 'hello'}));

    const interval = setInterval(() => {
        ws.send(JSON.stringify({channel: channels[_.random(0,2)], msg: messages[_.random(0, 5)]}));
    }, 30000);

    ws.on('close', () => clearInterval(interval));
});